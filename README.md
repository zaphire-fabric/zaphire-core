# Zaphire Core

Boilerplate for Zaphire, a Fabric based Framework for building plugin like mods.

## Features

* Multiple filetypes support
* Database support (Coming soon)
* Permissions support (Such as LuckPerms)
* Custom Events
* More to come soon...

## License

Zaphire Core is licensed under MIT

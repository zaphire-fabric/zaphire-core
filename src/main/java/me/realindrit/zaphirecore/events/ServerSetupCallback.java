package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;

public interface ServerSetupCallback {

    Event<ServerSetupCallback> EVENT = EventFactory.createArrayBacked(ServerSetupCallback.class, (callbacks) -> () -> {
        for (ServerSetupCallback callback : callbacks) {
            callback.onSetup();
        }
    });

    void onSetup();
}

package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.server.network.ServerPlayerEntity;

public interface PlayerFirstJoinCallback {

    Event<PlayerFirstJoinCallback> EVENT = EventFactory.createArrayBacked(PlayerFirstJoinCallback.class, (callbacks) -> (player) -> {
        for (PlayerFirstJoinCallback callback : callbacks) {
            callback.onFirstJoin(player);
        }
    });

    void onFirstJoin(ServerPlayerEntity player);
}
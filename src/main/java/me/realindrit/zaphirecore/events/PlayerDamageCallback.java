package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.server.network.ServerPlayerEntity;

public interface PlayerDamageCallback {

    Event<PlayerDamageCallback> EVENT = EventFactory.createArrayBacked(PlayerDamageCallback.class,
            (listeners) -> (playerId, damageSource, amount) -> {
                for (PlayerDamageCallback callback : listeners) {
                    boolean cancel  = callback.onPlayerDamaged(playerId, damageSource, amount);
                    if (cancel) {
                        return true;
                    }
                }
                return false;
            });

    boolean onPlayerDamaged(ServerPlayerEntity playerID, DamageSource damageSource, float amount);
}

package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.Packet;

public interface ClientConnectionPacketSendCallback {

    Event<ClientConnectionPacketSendCallback> EVENT = EventFactory.createArrayBacked(ClientConnectionPacketSendCallback.class,
            (listeners) -> (connectionn, player) -> {
                for (ClientConnectionPacketSendCallback listener : listeners) {
                    listener.onSendPacket(connectionn, player);
                }
            });
    void onSendPacket(ClientConnection connectionn, Packet<?> packet);
}

package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.fabricmc.fabric.api.util.TriState;
import net.minecraft.command.CommandSource;

import org.jetbrains.annotations.NotNull;

/**
 * Simple permissions check event for {@link CommandSource}s.
 */
public interface PermissionCheckCallback {

    Event<PermissionCheckCallback> EVENT = EventFactory.createArrayBacked(PermissionCheckCallback.class, (callbacks) -> (source, permission) -> {
        for (PermissionCheckCallback callback : callbacks) {
            TriState state = callback.onPermissionCheck(source, permission);
            if (state != TriState.DEFAULT) {
                return state;
            }
        }
        return TriState.DEFAULT;
    });

    @NotNull TriState onPermissionCheck(@NotNull CommandSource source, @NotNull String permission);

}
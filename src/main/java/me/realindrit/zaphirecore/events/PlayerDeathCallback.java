package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.server.network.ServerPlayerEntity;

public interface PlayerDeathCallback {

    Event<PlayerDeathCallback> EVENT = EventFactory.createArrayBacked(PlayerDeathCallback.class, (callbacks) -> (player, source) -> {
        for (PlayerDeathCallback callback : callbacks) {
            callback.onDeath(player, source);
        }
    });

    void onDeath(ServerPlayerEntity player, DamageSource source);
}
package me.realindrit.zaphirecore.events;

import com.mojang.authlib.GameProfile;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.text.Text;

import java.net.SocketAddress;

public interface PlayerPreJoinCallback {

    Event<PlayerPreJoinCallback> EVENT = EventFactory.createArrayBacked(PlayerPreJoinCallback.class, (callbacks) -> (address, profile) -> {
        for (PlayerPreJoinCallback callback : callbacks) {
            Text text = callback.onJoinCheck(address, profile);
            if (text != null) {
                return text;
            }
        }
        return null;
    });

    Text onJoinCheck(SocketAddress address, GameProfile profile);
}

package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.server.network.ServerPlayerEntity;

public interface PlayerJoinCallback {

    Event<PlayerJoinCallback> EVENT = EventFactory.createArrayBacked(PlayerJoinCallback.class, (callbacks) -> (player) -> {
        for (PlayerJoinCallback callback : callbacks) {
            callback.onJoin(player);
        }
    });

    void onJoin(ServerPlayerEntity player);
}
package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.Packet;

public interface ClientConnectionPacketRecieveCallback {

    Event<ClientConnectionPacketRecieveCallback> EVENT = EventFactory.createArrayBacked(ClientConnectionPacketRecieveCallback.class,
            (listeners) -> (connectionn, player) -> {
                for (ClientConnectionPacketRecieveCallback listener : listeners) {
                    listener.onRecievePacket(connectionn, player);
                }
            });
    void onRecievePacket(ClientConnection connectionn, Packet<?> packet);
}

package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.network.MessageType;
import net.minecraft.text.Text;

import java.util.UUID;

public interface MessageGameInfoCallback {
    Event<MessageGameInfoCallback> EVENT = EventFactory.createArrayBacked(MessageGameInfoCallback.class,
            (listeners) -> (serverMessage, type, sender) -> {
                for (MessageGameInfoCallback listener : listeners) {
                    boolean cancel = listener.onMessage(serverMessage, type, sender);
                    if(cancel) {
                        return true;
                    }
                }
                return false;
            });
    boolean onMessage(Text serverMessage, MessageType type, UUID sender);
}

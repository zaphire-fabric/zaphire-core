package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;

public interface ServerShutdownCallback {

    Event<ServerShutdownCallback> EVENT = EventFactory.createArrayBacked(ServerShutdownCallback.class, (callbacks) -> () -> {
        for (ServerShutdownCallback callback : callbacks) {
            callback.onShutdown();
        }
    });

    void onShutdown();
}
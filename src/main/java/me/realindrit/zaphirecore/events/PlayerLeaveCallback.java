package me.realindrit.zaphirecore.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;

public interface PlayerLeaveCallback {
    Event<PlayerLeaveCallback> EVENT = EventFactory.createArrayBacked(PlayerLeaveCallback.class, (callbacks) -> (player) -> {
        for (PlayerLeaveCallback callback : callbacks) {
            callback.onPlayerLeave(player);
        }
    });

    void onPlayerLeave(ServerPlayerEntity player);
}
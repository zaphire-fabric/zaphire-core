package me.realindrit.zaphirecore.events;

import com.mojang.authlib.GameProfile;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;

public interface PlayerLimitCheckCallback {

    Event<PlayerLimitCheckCallback> EVENT = EventFactory.createArrayBacked(PlayerLimitCheckCallback.class, (callbacks) -> (profile) -> {
        for (PlayerLimitCheckCallback callback : callbacks) {
            return callback.onPlayerLimitCheck(profile);
        }
        return false;
    });

    boolean onPlayerLimitCheck(GameProfile profile);
}

package me.realindrit.zaphirecore;

import me.realindrit.zaphirecore.utils.Logger;
import net.fabricmc.api.DedicatedServerModInitializer;
import net.minecraft.server.MinecraftServer;

public class ZaphireCore implements DedicatedServerModInitializer {
    public static final Logger logger =  new Logger("ZaphireCore");
    public static MinecraftServer server;
    //TODO: Add futurable executor service for async tasks with ability to sync with main thread (teleport cooldown, etc)

    public static final String zaphire_data = "./config/zaphire/";


    @Override
    public void onInitializeServer() {
        logger.info("Initializing ZaphireCore");
        logger.info("ZaphireCore has been initialized!");
    }
}

//https://github.com/123456687548/vanish
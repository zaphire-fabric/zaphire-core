package me.realindrit.zaphirecore.mixin.server;

import io.netty.channel.ChannelHandlerContext;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import me.realindrit.zaphirecore.events.ClientConnectionPacketRecieveCallback;
import me.realindrit.zaphirecore.events.ClientConnectionPacketSendCallback;
import me.realindrit.zaphirecore.events.PlayerLeaveCallback;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.Packet;
import net.minecraft.server.network.ServerPlayerEntity;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ClientConnection.class)
public class ClientConnectionMixin {

    @Inject(at = @At(value = "HEAD"), method = "sendImmediately", cancellable = true)
    private void onSendPacket(Packet<?> packet, @Nullable GenericFutureListener<? extends Future<? super Void>> listener, CallbackInfo info) {
        ClientConnection connection = (ClientConnection) (Object) this;
        ClientConnectionPacketSendCallback.EVENT.invoker().onSendPacket(connection, packet);
    }

    @Inject(at = @At(value = "HEAD"), method = "channelRead0(Lio/netty/channel/ChannelHandlerContext;Lnet/minecraft/network/Packet;)V", cancellable = true)
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet<?> packet, CallbackInfo info) {
        ClientConnection connection = (ClientConnection) (Object) this;
        ClientConnectionPacketRecieveCallback.EVENT.invoker().onRecievePacket(connection, packet);
    }
}

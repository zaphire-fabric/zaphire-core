package me.realindrit.zaphirecore.mixin.server;

import me.realindrit.zaphirecore.ZaphireCore;
import me.realindrit.zaphirecore.events.ServerSetupCallback;
import me.realindrit.zaphirecore.events.ServerShutdownCallback;
import net.minecraft.server.*;
import net.minecraft.server.dedicated.MinecraftDedicatedServer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MinecraftDedicatedServer.class)
public class MinecraftDedicatedServerMixin {
    @Inject(at = @At(value = "HEAD"), method = "shutdown")
    private  void shutdown(CallbackInfo info) {
        ServerShutdownCallback.EVENT.invoker().onShutdown();
    }

    @Inject(at = @At(value = "TAIL"), method = "setupServer")
    private void setupServer(CallbackInfoReturnable<Boolean> cir) {
        ZaphireCore.server = (MinecraftServer) (Object) this;
        ServerSetupCallback.EVENT.invoker().onSetup();
    }
}

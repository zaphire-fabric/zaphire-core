package me.realindrit.zaphirecore.mixin.player;

import com.mojang.authlib.GameProfile;
import me.realindrit.zaphirecore.events.PlayerLimitCheckCallback;
import net.minecraft.server.dedicated.DedicatedPlayerManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(DedicatedPlayerManager.class)
public class DedicatedPlayerManagerMixin {
    @Inject(at = @At(value = "HEAD"), method = "canBypassPlayerLimit", cancellable = true)
    private void onPlayerLimitCheck(GameProfile profile, CallbackInfoReturnable<Boolean> cir) {
        if(PlayerLimitCheckCallback.EVENT.invoker().onPlayerLimitCheck(profile)) {
            cir.setReturnValue(true);
        }
    }
}

package me.realindrit.zaphirecore.mixin.player;


import com.mojang.datafixers.util.Either;
import me.realindrit.zaphirecore.events.*;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.MessageType;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Unit;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.UUID;

@Mixin(ServerPlayerEntity.class)
public abstract class ServerPlayerEntityMixin {
    @Shadow public abstract Either<PlayerEntity.SleepFailureReason, Unit> trySleep(BlockPos pos);

    @Inject(at = @At(value = "TAIL"), method = "onDeath")
    private void onPlayerDeath(DamageSource source, CallbackInfo info) {
        ServerPlayerEntity player = (ServerPlayerEntity) (Object) this;
        PlayerDeathCallback.EVENT.invoker().onDeath(player, source);
    }

    @Inject(at = @At(value = "HEAD"), method = "damage")
    private void onPlayerDamage(DamageSource source, float amount, CallbackInfoReturnable<Boolean> info) {
        ServerPlayerEntity player = (ServerPlayerEntity) (Object) this;
        if(PlayerDamageCallback.EVENT.invoker().onPlayerDamaged(player, source, amount)){
            info.setReturnValue(true);
        }
    }

    @Inject(at = @At(value = "HEAD"), method = "onDisconnect")
    private void onPlayerDisconnect(CallbackInfo info) {
        ServerPlayerEntity player = (ServerPlayerEntity) (Object) this;
        PlayerLeaveCallback.EVENT.invoker().onPlayerLeave(player);
    }

    @Inject(at = @At(value = "HEAD"), method = "sendMessage(Lnet/minecraft/text/Text;Lnet/minecraft/network/MessageType;Ljava/util/UUID;)V", cancellable = true)
    private void onBroadcast(Text message, MessageType type, UUID sender, CallbackInfo ci) {
        switch (type) {
            case CHAT -> {
                if (MessageChatCallback.EVENT.invoker().onMessage(message, type, sender)) {
                    ci.cancel();
                }
            }
            case SYSTEM -> {
                if (MessageSystemCallback.EVENT.invoker().onMessage(message, type, sender)) {
                    ci.cancel();
                }
            }
            case GAME_INFO -> {
                if (MessageGameInfoCallback.EVENT.invoker().onMessage(message, type, sender)) {
                    ci.cancel();
                }
            }
        }
    }
}
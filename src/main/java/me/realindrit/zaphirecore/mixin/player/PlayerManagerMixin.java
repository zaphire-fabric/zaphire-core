package me.realindrit.zaphirecore.mixin.player;

import com.mojang.authlib.GameProfile;
import me.realindrit.zaphirecore.events.*;
import net.minecraft.network.ClientConnection;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.stat.Stats;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.net.SocketAddress;

@Mixin(PlayerManager.class)

public class PlayerManagerMixin {
    //https://github.com/ByMartrixx/player-events
    // We inject right after the vanilla player join message is sent. Mostly to ensure LuckPerms permissions are
    // loaded (for role styling in EC MOTD).

    @Inject(at = @At("TAIL"), method = "onPlayerConnect")
    private void onPlayerJoin(ClientConnection connection, ServerPlayerEntity player, CallbackInfo ci) {
        PlayerJoinCallback.EVENT.invoker().onJoin(player);
        if (player.getStatHandler().getStat(Stats.CUSTOM.getOrCreateStat(Stats.LEAVE_GAME)) < 1) {
            PlayerFirstJoinCallback.EVENT.invoker().onFirstJoin(player);
        }
    }

    // We inject just before all "pre join checks" are done for a player. (Allows for custom join restrictions)
    @Inject(at = @At(value = "TAIL"), method = "checkCanJoin", cancellable = true)
    private void onPlayerPreJoin(SocketAddress address, GameProfile profile, CallbackInfoReturnable<Text> cir) {
        cir.setReturnValue(PlayerPreJoinCallback.EVENT.invoker().onJoinCheck(address, profile));
    }
/*
    @Inject(method = "respawnPlayer", at = @At(value ="INVOKE", target = "ServerPlayerEntity serverPlayerEntity = new ServerPlayerEntity(this.server, serverWorld2, player.getGameProfile());"))
    private void afterRespawn(ServerPlayerEntity oldPlayer, boolean alive, CallbackInfoReturnable<ServerPlayerEntity> cir) {
        ServerPlayerEvents.AFTER_RESPAWN.invoker().afterRespawn(oldPlayer, cir.getReturnValue(), alive);
    }
    */
}
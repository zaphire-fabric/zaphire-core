package me.realindrit.zaphirecore.utils;

import org.apache.logging.log4j.LogManager;

import java.time.format.DateTimeFormatter;

public class Logger {
    public static final DateTimeFormatter onlineTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private org.apache.logging.log4j.Logger logger;
    public Logger(String name) {
        logger = LogManager.getLogger(name);
    }
    public void info(String msg, Object... o) {
        logger.info(msg, o);
    }

    public void debug(String msg, Object... o) {
        logger.debug(msg, o);
    }

    public void warn(String msg, Object... o) {
        logger.warn(msg, o);
    }

    public void error(String msg, Object... o) {
        logger.error(msg, o);
    }

}

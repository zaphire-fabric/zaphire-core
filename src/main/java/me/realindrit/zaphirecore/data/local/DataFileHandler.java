package me.realindrit.zaphirecore.data.local;

import java.io.*;

public interface DataFileHandler {
    public void loadData();
    public void saveData();
    public File getDataFile();
}
//https://mkyong.com/java/java-properties-file-examples/

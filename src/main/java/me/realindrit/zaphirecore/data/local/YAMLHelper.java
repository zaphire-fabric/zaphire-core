package me.realindrit.zaphirecore.data.local;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class YAMLHelper implements DataFileHandler {
    private final Yaml yaml;
    private final File file;

    private Map<String, Object> yaml_data;

    public YAMLHelper(String file_path) throws IOException {
        this.file = new File(file_path);
        file.getParentFile().mkdirs();
        file.createNewFile();
        this.yaml = new Yaml();
    }

    public YAMLHelper(File file) throws IOException {
        this.file = file;
        file.getParentFile().mkdirs();
        file.createNewFile();
        this.yaml = new Yaml();
    }

    @Override
    public void loadData() {
        try {
            yaml_data = this.yaml.load(new FileInputStream(this.file));
            if (yaml_data == null) {
                yaml_data = new HashMap<String, Object>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveData() {
        try {
            this.yaml.dump(yaml_data, new FileWriter(this.file));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setData(String key, Object value) {
        yaml_data.put(key, value);
    }

    public void removeData(String key) {
        yaml_data.remove(key);
    }

    public void clearData() {
        yaml_data.clear();
    }

    public Object getData(String key) {
        return yaml_data.get(key);
    }

    public boolean hasData(String key) {
        return yaml_data.containsKey(key);
    }

    public Map<String, Object> getYamlMap() {
        return yaml_data;
    }

    public void setYamlMap(Map<String, Object> yaml_data) {
        this.yaml_data = yaml_data;
    }

    @Override
    public File getDataFile() {
        return this.file;
    }
}

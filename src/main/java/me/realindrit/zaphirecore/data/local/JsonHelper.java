package me.realindrit.zaphirecore.data.local;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import me.realindrit.zaphirecore.ZaphireCore;
import org.jetbrains.annotations.NotNull;

import java.io.*;

public class JsonHelper implements DataFileHandler {
    private File file;
    private JsonObject jsonObject;

    public JsonHelper(String file_path) throws IOException {
        this.file = new File(file_path);
        file.getParentFile().mkdirs();
        file.createNewFile();
        this.jsonObject = new JsonObject();
    }

    public JsonHelper(File file) throws IOException {
        this.file = file;
        file.getParentFile().mkdirs();
        file.createNewFile();
        this.jsonObject = new JsonObject();
    }

    @Override
    public void loadData() {
        try {
            if (this.file.exists()) {
                final Gson gson = new GsonBuilder().setPrettyPrinting().create();
                JsonObject json = gson.fromJson(new FileReader(this.file), JsonObject.class);
                this.jsonObject = json == null ? jsonObject : json;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ZaphireCore.logger.info("Loaded data from " + this.file.getName());
    }

    @Override
    public void saveData() {
        try {
            final PrintWriter writer = new PrintWriter(new FileWriter(this.file));
            final Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(this.jsonObject, writer);
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ZaphireCore.logger.info("Saved data to " + this.file.getName());
    }

    @Override
    public File getDataFile() {
        return this.file;
    }

    public JsonObject getJsonObject() {
        return this.jsonObject;
    }

    public void setJsonObject(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}

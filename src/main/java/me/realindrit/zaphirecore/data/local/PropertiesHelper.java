package me.realindrit.zaphirecore.data.local;

import java.io.*;
import java.util.Properties;

public class PropertiesHelper implements DataFileHandler {
    private final Properties properties;
    private final File file;

    public PropertiesHelper(String file_path) throws IOException {
        this.file = new File(file_path);
        file.getParentFile().mkdirs();
        file.createNewFile();
        this.properties = new Properties();
    }

    public PropertiesHelper(File file) throws IOException {
        this.file = file;
        file.getParentFile().mkdirs();
        file.createNewFile();
        this.properties = new Properties();
    }

    @Override
    public void loadData() {
        try {
            this.properties.load(new FileInputStream(this.file));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveData() {
        try {
            this.properties.store(new FileWriter(this.file), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setData(String key, Object value) {
        this.properties.setProperty(key, (String) value);
    }

    public String getData(String key) {
        return this.properties.getProperty(key);
    }

    public void removeData(String key) {
        this.properties.remove(key);
    }

    @Override
    public File getDataFile() {
        return this.file;
    }

    public void clearData() {
        this.properties.clear();
    }
}

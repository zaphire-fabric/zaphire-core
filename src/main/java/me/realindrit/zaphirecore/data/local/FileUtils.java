package me.realindrit.zaphirecore.data.local;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    public static List<File> getFilesInDir(String dir) {
        File file = new File(dir);
        List<File> files = new ArrayList<>();
        if (file.exists() && file.listFiles() != null) {
            for (File f : file.listFiles()) {
                if (!f.isDirectory()) {
                    files.add(f);
                }
            }
        }
        return files;
    }
}
